import re
msg = input("write password")
def minlen(msg):
    if len(msg) < 8:
        print("error: too short, min length is 8")
    if not re.search('[A-Z]+', msg):
        print("error: should contain at least one uppercase letter")
    if not re.search('\d+', msg):
        print("error: should contain at least one digit")
    else:
        exit(0)
minlen(msg)
